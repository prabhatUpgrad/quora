package com.upgrad.sdproject.sdproject.configuration.security;

import com.upgrad.sdproject.sdproject.utils.SecurityUtil;
import com.upgrad.sdproject.sdproject.web.entities.User;
import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

/**
 * @author prabhat on 14/04/18
 */
public class AuditorAwareImpl implements AuditorAware<String> {

	@Override public Optional<String> getCurrentAuditor() {
		User user = SecurityUtil.getUser();
		return user == null ? Optional.of("anonymous") : Optional.of(user.getEmail());
	}
}
