package com.upgrad.sdproject.sdproject.configuration.security;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author prabhat on 01/04/18
 */
@Component
public class AuthFailureHandler extends SimpleUrlAuthenticationFailureHandler {

	public static String getMessage(String code) {
		return code;
	}

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) {
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
	}
}
