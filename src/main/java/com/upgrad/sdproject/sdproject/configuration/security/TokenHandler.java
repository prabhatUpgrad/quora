package com.upgrad.sdproject.sdproject.configuration.security;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.upgrad.sdproject.sdproject.utils.DateUtil;
import com.upgrad.sdproject.sdproject.utils.JsonUtil;
import com.upgrad.sdproject.sdproject.web.entities.SessionToken;
import com.upgrad.sdproject.sdproject.web.entities.User;
import com.upgrad.sdproject.sdproject.web.services.sessionToken.SessionTokenService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author prabhat on 31/03/18
 */
@Component
@Slf4j
public class TokenHandler {

	private static final String SECRETKEY = "HALLELUJAH";
	private static SessionTokenService sessionTokenService;
	private final SessionTokenService sessionTokenServiceTemp;

	@Autowired public TokenHandler(SessionTokenService sessionTokenServiceTemp) {
		this.sessionTokenServiceTemp = sessionTokenServiceTemp;
	}

	public static String createTokenForUser(User user, String sessionID) throws JsonProcessingException {
		String token = getToken(user, sessionID);
		SessionToken sessionToken = new SessionToken(token, user);

		if (user.hasRole("ROLE_ADMIN")) {
			sessionToken.setCreated(DateUtil.parseDate("01/01/2050"));
		}
		sessionToken.setLastSessionId(sessionID);
		try {
			sessionTokenService.updateSessionToken(sessionToken);
		} catch (Exception e) {
			/* IGNORE */
			log.error("Error in saving session token", e);
		}
		return token;
	}

	public static String getToken(User user, String sessionID) throws JsonProcessingException {
		Map<String, String> responseMap = new HashMap<String, String>();
		responseMap.put("username", user.getUserName());
		responseMap.put("sessionID", sessionID);
		String jsonString = JsonUtil.toJson(responseMap);
		return Jwts.builder().setSubject(jsonString).signWith(SignatureAlgorithm.HS512, TokenHandler.SECRETKEY).compact();
	}

	public static Boolean exists(User user, String sessionID) throws JsonProcessingException {
		String token = getToken(user, sessionID);
		return sessionTokenService.findSessionTokensByTokenEquals(token) != null;
	}

	@PostConstruct
	void init() {
		sessionTokenService = sessionTokenServiceTemp;
	}
}
