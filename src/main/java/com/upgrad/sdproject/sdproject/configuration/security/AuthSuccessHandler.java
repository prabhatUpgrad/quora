package com.upgrad.sdproject.sdproject.configuration.security;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.upgrad.sdproject.sdproject.web.services.userDetails.UpGradUserDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author prabhat on 31/03/18
 */
@Component
@Slf4j
public class AuthSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

	private static final String HEADER_TOKEN = "Auth-Token";
	private static final String HEADER_SESSION_ID = "sessionID";
	private static final String LOGINPATH = "/login";

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws JsonProcessingException {
		UpGradUserDetails userDetails = (UpGradUserDetails) authentication.getPrincipal();
		String sessionId = request.getHeader(AuthSuccessHandler.HEADER_SESSION_ID);
		log.debug("Session-Id is : " + sessionId);

		if (isLogin(request) && (sessionId != null)) {
			if (TokenHandler.exists(userDetails.getUser(), sessionId)) {
				response.setHeader(AuthSuccessHandler.HEADER_TOKEN, TokenHandler.getToken(userDetails.getUser(), sessionId));
				return;
			}
			String token = TokenHandler.createTokenForUser(userDetails.getUser(), sessionId);
			log.debug("Creating token : " + token);
			response.setHeader(AuthSuccessHandler.HEADER_TOKEN, token);
		}
		response.setStatus(HttpServletResponse.SC_OK);
	}

	private boolean isLogin(HttpServletRequest request) {
		if (currentLink(request).equals(LOGINPATH)) {
			return true;
		}
		return false;
	}

	private String currentLink(HttpServletRequest httpRequest) {
		if (httpRequest.getPathInfo() == null) {
			return httpRequest.getServletPath();
		}
		return httpRequest.getServletPath() + httpRequest.getPathInfo();
	}
}
