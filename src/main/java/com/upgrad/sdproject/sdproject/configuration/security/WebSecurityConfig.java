package com.upgrad.sdproject.sdproject.configuration.security;

import com.upgrad.sdproject.sdproject.web.services.sessionToken.SessionTokenService;
import com.upgrad.sdproject.sdproject.web.services.userDetails.UpGradUserDetailsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.http.HttpServletResponse;

/**
 * @author prabhat on 31/03/18
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
@Slf4j
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	private final BCryptPasswordEncoder bCryptPasswordEncoder;

	private final SessionTokenService sessionTokenService;

	private final UpGradUserDetailsService userDetailsService;

	private final AuthenticationSuccessHandler authenticationSuccessHandler;

	private final AuthenticationFailureHandler authenticationFailureHandler;

	private final RestLogoutSuccessHandler logoutSuccessHandler;

	@Autowired
	public WebSecurityConfig(BCryptPasswordEncoder bCryptPasswordEncoder,
			SessionTokenService sessionTokenService, UpGradUserDetailsService userDetailsService,
			AuthenticationSuccessHandler authenticationSuccessHandler,
			AuthenticationFailureHandler authenticationFailureHandler,
			RestLogoutSuccessHandler logoutSuccessHandler) {
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.sessionTokenService = sessionTokenService;
		this.userDetailsService = userDetailsService;
		this.authenticationSuccessHandler = authenticationSuccessHandler;
		this.authenticationFailureHandler = authenticationFailureHandler;
		this.logoutSuccessHandler = logoutSuccessHandler;
	}

	@Override
	public void configure(WebSecurity web) {
		web.ignoring().antMatchers("/resources/**")
				.antMatchers("/swagger-resources/**")
				.antMatchers("/webjars/**")
				.antMatchers("/v2/api-docs")
				.antMatchers("/configuration/**")
				.antMatchers("/swagger-ui.html")
				.antMatchers("/signup");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.exceptionHandling().authenticationEntryPoint(unauthorizedEntryPoint())
				.and().csrf()
				.disable()
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				.and()
				.authorizeRequests()
				.antMatchers("/login").permitAll()
				.antMatchers("/signup").permitAll()
				.anyRequest().authenticated()
				.and().logout().logoutSuccessHandler(logoutSuccessHandler);

		http.addFilterAt(authTokenFilter(), UsernamePasswordAuthenticationFilter.class);
	}

	private AuthenticationFilter authTokenFilter() throws Exception {
		AuthenticationFilter tokenAuthenticationFilter = new AuthenticationFilter("/**",
				sessionTokenService, userDetailsService);
		tokenAuthenticationFilter.setAuthenticationManager(authenticationManager());
		tokenAuthenticationFilter.setAuthenticationSuccessHandler(authenticationSuccessHandler);
		tokenAuthenticationFilter.setAuthenticationFailureHandler(authenticationFailureHandler);
		return tokenAuthenticationFilter;
	}

	@Bean
	public AuthenticationEntryPoint unauthorizedEntryPoint() {
		return (request, response, authException) -> {
			log.error(authException.getMessage(), authException);
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
		};
	}

	@Override
	@Autowired
	public void configure(AuthenticationManagerBuilder authBuilder) throws Exception {
		authBuilder.userDetailsService(userDetailsService)
				.passwordEncoder(bCryptPasswordEncoder);
	}
}
