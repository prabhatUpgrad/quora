package com.upgrad.sdproject.sdproject.configuration.security;

import com.upgrad.sdproject.sdproject.web.entities.SessionToken;
import com.upgrad.sdproject.sdproject.web.services.sessionToken.SessionTokenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author prabhat on 01/04/18
 */
@Component
@Slf4j
public class RestLogoutSuccessHandler implements LogoutSuccessHandler {
	private static final String HEADER_TOKEN = "Auth-Token";

	private final SessionTokenService sessionTokenService;

	@Autowired public RestLogoutSuccessHandler(SessionTokenService sessionTokenService) {
		this.sessionTokenService = sessionTokenService;
	}

	@Override public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		String token = request.getHeader(RestLogoutSuccessHandler.HEADER_TOKEN);
		if (token != null) {
			log.debug("Clearing token on logout :" + token);
			response.setHeader(RestLogoutSuccessHandler.HEADER_TOKEN, null);
			try {
				SessionToken sessionToken = sessionTokenService.findSessionTokensByTokenEquals(token);
				if (sessionToken != null) {
					sessionTokenService.deleteSessionToken(sessionToken);
				}
			} catch (EmptyResultDataAccessException e) {
				log.error("No token found in the dataBase: " + token);
			}

		}
		response.setStatus(HttpServletResponse.SC_OK);
	}
}
