package com.upgrad.sdproject.sdproject.configuration;

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author prabhat on 30/03/18
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.upgrad.sdproject.sdproject.web.controllers"))
				.paths(PathSelectors.any())
				.build()
				.apiInfo(apiInfo())
				.securitySchemes(Lists.newArrayList(apiKey()))
				.securityContexts(Lists.newArrayList(securityContext())).select()
				.paths(apiPaths()).build();
	}

	private ApiKey apiKey() {
		return new ApiKey("AUTH-TOKEN", "AUTH-TOKEN", "header");
	}

	private SecurityContext securityContext() {
		return SecurityContext.builder().forPaths(PathSelectors.any()).build();
	}

	private Predicate<String> apiPaths() {
		return PathSelectors.any();
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("Upgrad-quora")
				.description("quora replica for SDE course")
				.termsOfServiceUrl("http://localhost:8080/")
				.version("1.0")
				.build();
	}
}
