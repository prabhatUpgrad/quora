package com.upgrad.sdproject.sdproject.configuration.security;

import com.google.common.base.Strings;
import com.upgrad.sdproject.sdproject.utils.Constants;
import com.upgrad.sdproject.sdproject.utils.JsonPath;
import com.upgrad.sdproject.sdproject.web.entities.SessionToken;
import com.upgrad.sdproject.sdproject.web.entities.User;
import com.upgrad.sdproject.sdproject.web.services.sessionToken.SessionTokenService;
import com.upgrad.sdproject.sdproject.web.services.userDetails.UpGradUserDetails;
import com.upgrad.sdproject.sdproject.web.services.userDetails.UpGradUserDetailsService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.util.CollectionUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

/**
 * @author prabhat on 31/03/18
 */
@Slf4j
public class AuthenticationFilter extends AbstractAuthenticationProcessingFilter {

	private static final String HEADER_TOKEN = "Auth-Token";
	private static final String LOGINPATH = "/login";
	private static final boolean POST_ONLY = true;
	private SessionTokenService sessionTokenService;
	private UpGradUserDetailsService userDetailsService;

	public AuthenticationFilter(String defaultFilterProcessesUrl, SessionTokenService sessionTokenService, UpGradUserDetailsService userDetailsService) {
		super(defaultFilterProcessesUrl);
		super.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher(defaultFilterProcessesUrl));
		this.sessionTokenService = sessionTokenService;
		this.userDetailsService = userDetailsService;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
		if ("OPTIONS".equals(request.getMethod())) {
			return null;
		}
		setAnonymousMDC();
		setSessionParams(request);
		try {
			if (isLogin(request)) {
				return loginattemptAuthentication(request);
			}

			String token = request.getHeader(AuthenticationFilter.HEADER_TOKEN);
			if (Strings.isNullOrEmpty(token) || "null".equalsIgnoreCase(token)) {
				throw new UsernameNotFoundException("No token found");
			}
			AbstractAuthenticationToken userAuthenticationToken = authUserByToken(token, request);
			if (userAuthenticationToken == null) {
				throw new InternalAuthenticationServiceException("Invalid Token : " + token);
			}
			setMDC(userAuthenticationToken);
			return userAuthenticationToken;
		} catch (IOException e) {
			log.error("Error while authenticating", e);
		}
		return null;
	}

	private AbstractAuthenticationToken authUserByToken(String token, HttpServletRequest request) {

		try {
			SessionToken sessionToken = sessionTokenService.findSessionTokensByTokenEquals(token);
			if (sessionToken == null) {
				return null;
			}

			if (sessionToken.isExpired()) {
				sessionTokenService.deleteSessionToken(sessionToken);
				return null;
			}
			String sessionId = request.getHeader(Constants.LogstashConstants.SESSION_ID);
			if ((sessionId != null) && !sessionId.equals(sessionToken.getLastSessionId())) {
				sessionToken.setLastSessionId(sessionId);
				try {
					sessionTokenService.updateSessionToken(sessionToken);
				} catch (Exception e) {
					log.error("Exception during saving the new user session id ");
				}
			}
			String username = sessionToken.getUser().getEmail();
			UpGradUserDetails myUserDetails = (UpGradUserDetails) userDetailsService.loadUserByUsername(username);
			return new UsernamePasswordAuthenticationToken(myUserDetails, "", myUserDetails.getAuthorities());
		} catch (EmptyResultDataAccessException e) {
			return null;
		}

	}

	public Authentication loginattemptAuthentication(HttpServletRequest request) throws IOException {
		if (AuthenticationFilter.POST_ONLY && !"POST".equals(request.getMethod())) {
			throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
		}

		String requestJson = getBody(request);
		JsonPath jsonPath = new JsonPath(requestJson);
		try {
			String username = jsonPath.readNotNull("$.username");
			String password = jsonPath.readNotNull("$.password");
			UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(username,
					password);
			// Allow subclasses to set the "details" property
			setDetails(request, authRequest);
			return getAuthenticationManager().authenticate(authRequest);
		} catch (IllegalArgumentException e) {
			throw new AuthenticationServiceException("No email or password entered");
		}
	}

	protected void setDetails(HttpServletRequest request, UsernamePasswordAuthenticationToken authRequest) {
		authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
	}

	public String getBody(HttpServletRequest request) throws IOException {
		// Read from request
		StringBuilder buffer = new StringBuilder();
		BufferedReader reader = request.getReader();
		String line;
		while ((line = reader.readLine()) != null) {
			buffer.append(line);
		}
		return buffer.toString();
	}

	private boolean isLogin(HttpServletRequest request) {
		return currentLink(request).equals(AuthenticationFilter.LOGINPATH);
	}

	private String currentLink(HttpServletRequest httpRequest) {
		if (httpRequest.getPathInfo() == null) {
			return httpRequest.getServletPath();
		}
		return httpRequest.getServletPath() + httpRequest.getPathInfo();
	}

	private void setAnonymousMDC() {
		MDC.put(Constants.LogstashConstants.USER_ID, Constants.LogstashConstants.DEFAULT_NUMBER);
		MDC.put(Constants.LogstashConstants.EMAIL, Constants.LogstashConstants.DEFAULT_EMAIL);
		MDC.put(Constants.LogstashConstants.ROLE, Constants.LogstashConstants.DEFAULT_STRING);
		MDC.put(Constants.LogstashConstants.COURSE_ID, Constants.LogstashConstants.DEFAULT_NUMBER);
		MDC.put(Constants.LogstashConstants.PLATFORM, Constants.LogstashConstants.DEFAULT_STRING);
		MDC.put(Constants.LogstashConstants.DEVICE_TYPE, Constants.LogstashConstants.DEFAULT_STRING);
		MDC.put(Constants.LogstashConstants.OPERATING_SYSTEM, Constants.LogstashConstants.DEFAULT_STRING);
		MDC.put(Constants.LogstashConstants.BROWSER_INFO, Constants.LogstashConstants.DEFAULT_STRING);
	}

	private void setMDC(AbstractAuthenticationToken userAuthenticationToken) {
		UpGradUserDetails userDetails = (UpGradUserDetails) userAuthenticationToken.getPrincipal();
		User user = userDetails.getUser();
		MDC.put(Constants.LogstashConstants.USER_ID, user == null ? Constants.LogstashConstants.DEFAULT_NUMBER : String.valueOf(user.getId()));
		MDC.put(Constants.LogstashConstants.EMAIL, user == null ? Constants.LogstashConstants.DEFAULT_EMAIL : user.getEmail());
		MDC.put(Constants.LogstashConstants.ROLE, user == null ? Constants.LogstashConstants.DEFAULT_STRING : String.valueOf(user.getRoles()));
	}

	private void setSessionParams(HttpServletRequest request) {
		String sessionId = request.getHeader(Constants.LogstashConstants.SESSION_ID);
		String requestId = request.getHeader(Constants.LogstashConstants.REQUEST_ID);
		String distinctId = request.getHeader(Constants.LogstashConstants.DISTINCT_ID);
		if (Strings.isNullOrEmpty(sessionId)) {
			sessionId = "nullSessionidsession";
		}
		if (Strings.isNullOrEmpty(requestId)) {
			requestId = "nullRequestIdSession";
		}
		if (Strings.isNullOrEmpty(distinctId)) {
			distinctId = "nullDistinctIdSession";
		}
		MDC.put(Constants.LogstashConstants.SESSION_ID, sessionId.replaceAll(" ", ""));
		MDC.put(Constants.LogstashConstants.REQUEST_ID, requestId.replaceAll(" ", ""));
		MDC.put(Constants.LogstashConstants.DISTINCT_ID, distinctId.replaceAll(" ", ""));

		String ipAddress = request.getHeader(Constants.LogstashConstants.HEADER_CLIENT_IP);
		if (Strings.isNullOrEmpty(ipAddress)) {
			ipAddress = request.getRemoteAddr();
		}
		if ("0:0:0:0:0:0:0:1".equals(ipAddress)) {
			MDC.put(Constants.LogstashConstants.IP_ADDRESS, "127.0.0.1");
		} else {
			MDC.put(Constants.LogstashConstants.IP_ADDRESS, ipAddress.split(",")[0].replaceAll(" ", ""));
		}
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		super.doFilter(req, res, chain);

		if (!requiresAuthentication(request, response)) {
			return;
		}
		chain.doFilter(req, res);

		// do post controller work here
		setAnonymousMDC();
	}
}
