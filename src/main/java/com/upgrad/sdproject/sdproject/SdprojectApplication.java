package com.upgrad.sdproject.sdproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@SpringBootApplication
public class SdprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SdprojectApplication.class, args);
	}

	@PostConstruct
	public void init() {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));   // It will set our application to run in UTC timezone by default
	}
}
