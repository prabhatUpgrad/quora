package com.upgrad.sdproject.sdproject.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.upgrad.sdproject.sdproject.utils.exceptions.JsonParsingException;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.List;

import static com.upgrad.sdproject.sdproject.utils.Constants.ErrorConstants.JSON_CONVERSION_ERROR;

/**
 * @author prabhat on 13/04/18
 */
@Slf4j
public class JsonUtil {

	private static final JsonUtil.JsonUtilConfigurable instance;

	static {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.disable(MapperFeature.DEFAULT_VIEW_INCLUSION);

		instance = new JsonUtil.JsonUtilConfigurable(mapper);
	}

	public static JsonUtil.JsonUtilConfigurable getConfiguration() {
		return instance;
	}

	public static <E> E fromJson(String json, Class<E> type) {
		return instance.fromJson(json, type);
	}

	public static <E> E fromJson(String json, TypeReference<E> valueTypeRef) {
		return instance.fromJson(json, valueTypeRef);
	}

	public static <E> List<E> fromJsonList(String json, Class<E> type) {
		return instance.fromJsonList(json, type);
	}

	public static String toJson(Object object) {
		return instance.toJson(object);
	}

	public static String toPrettyJson(Object object) {
		return instance.toPrettyJson(object);
	}

	public static String patchJson(String originalJson, String patchJson) {
		return instance.patchJson(originalJson, patchJson);
	}

	public static class JsonUtilConfigurable {
		final ObjectMapper mapper;

		JsonUtilConfigurable(ObjectMapper mapper) {
			this.mapper = mapper;
		}

		public ObjectMapper getMapper() {
			return mapper;
		}

		<E> E fromJson(String json, Class<E> type) {
			return fromJsonInternal(json, content -> mapper.readValue(content, type));
		}

		<E> List<E> fromJsonList(String json, Class<E> type) {
			return fromJsonInternal(json, content -> mapper.readValue(content, mapper.getTypeFactory().constructCollectionType(List.class, type)));
		}

		<E> E fromJson(String json, TypeReference<E> type) {
			return fromJsonInternal(json, content -> mapper.readValue(content, type));
		}

		private <T> T fromJsonInternal(String json, ThrowableFunction<String, T> valueTypeRefFunction) {
			if (json == null || "".equals(json.trim()) || "\"\"".equals(json.trim())) {
				return null;
			}
			try {
				return valueTypeRefFunction.apply(json);
			} catch (IOException e) {
				log.error(JSON_CONVERSION_ERROR, e);
				throw new JsonParsingException(e);
			}
		}

		String toJson(Object object) {
			try {
				return mapper.writeValueAsString(object);
			} catch (IOException e) {
				log.error(JSON_CONVERSION_ERROR, e);
				throw new JsonParsingException(e);
			}
		}

		String toPrettyJson(Object object) {
			try {
				return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
			} catch (IOException e) {
				log.error(JSON_CONVERSION_ERROR, e);
				throw new JsonParsingException(e);
			}
		}

		String patchJson(String originalJson, String patchJson) {
			JsonNode updatedNode;
			try {
				JsonNode originalNode = mapper.readTree(originalJson);
				JsonNode patchNode = mapper.readTree(patchJson);
				updatedNode = merge(originalNode, patchNode);
			} catch (IOException e) {
				log.error(JSON_CONVERSION_ERROR, e);
				return null;
			}

			StringWriter writer = new StringWriter();
			try {
				JsonGenerator gen = mapper.getFactory().createGenerator(writer);
				mapper.writeTree(gen, updatedNode);
			} catch (IOException e) {
				log.error(JSON_CONVERSION_ERROR, e);
				throw new JsonParsingException(e);
			}

			return writer.toString();
		}

		JsonNode merge(JsonNode originalNode, JsonNode patchNode) {
			Iterator<String> fieldsToBeUpdated = patchNode.fieldNames();

			while (fieldsToBeUpdated.hasNext()) {
				String fieldToBeUpdated = fieldsToBeUpdated.next();
				JsonNode valueToBeUpdated = originalNode.get(fieldToBeUpdated);
				JsonNode updatedValue = patchNode.get(fieldToBeUpdated);

				// if the node is an ArrayNode
				if (valueToBeUpdated != null && updatedValue.isArray()) {
					for (int i = 0; i < updatedValue.size(); i++) {
						JsonNode updatedChildNode = updatedValue.get(i);
						if (valueToBeUpdated.size() <= i) {
							((ArrayNode) valueToBeUpdated).add(updatedChildNode);
						}
						JsonNode childNodeToBeUpdated = valueToBeUpdated.get(i);
						merge(childNodeToBeUpdated, updatedChildNode);
					}
				}
				// if the node is an ObjectNode
				else if (valueToBeUpdated != null && valueToBeUpdated.isObject()) {
					merge(valueToBeUpdated, updatedValue);
				} else if (originalNode instanceof ObjectNode) {
					((ObjectNode) originalNode).replace(fieldToBeUpdated, updatedValue);
				}
			}

			return originalNode;
		}
	}
}
