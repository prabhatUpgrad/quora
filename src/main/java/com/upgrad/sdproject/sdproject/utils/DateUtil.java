package com.upgrad.sdproject.sdproject.utils;

import com.google.common.base.Strings;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * @author prabhat on 31/03/18
 */
public final class DateUtil {

	public static final String DATE_FORMAT_DDMMYYYY = "dd/MM/yyyy";

	public static Date parseDate(String dateString) {
		if (Strings.isNullOrEmpty(dateString)) {
			return null;
		}
		DateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT_DDMMYYYY);
		dateFormatter.setTimeZone(TimeZone.getTimeZone("IST"));
		Date date = null;
		try {
			date = dateFormatter.parse(dateString);
		} catch (ParseException e) {
			/* Ignore */
		}
		return date;
	}
}
