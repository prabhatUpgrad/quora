package com.upgrad.sdproject.sdproject.utils;

import java.io.IOException;

/**
 * Using custom interface because java8's existing Function interface doesn't throw exception.
 * @author prabhat on 13/04/18
 */
@FunctionalInterface
public interface ThrowableFunction<T, R> {
	R apply(T t) throws IOException;
}
