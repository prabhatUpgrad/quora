package com.upgrad.sdproject.sdproject.utils;

import com.jayway.jsonpath.DocumentContext;

/**
 * @author prabhat on 01/04/18
 */
public class JsonPath {
	private String jsonString;
	private DocumentContext ctx;

	public JsonPath(String jsonString) {
		super();
		this.jsonString = jsonString;
		this.ctx = com.jayway.jsonpath.JsonPath.parse(this.jsonString);
	}

	public <T> T read(String path) {
		try {
			return this.ctx.read(path);
		} catch (Exception e) {
			return null;
		}
	}

	public <T> T read(String path, T defaultValue) {
		try {
			return this.ctx.read(path);
		} catch (Exception e) {
			return defaultValue;
		}
	}

	public Double readDouble(String path, Double defaultValue) {
		try {
			return this.ctx.read(path, Double.class);
		} catch (Exception e) {
			return defaultValue;
		}
	}

	public Integer readInteger(String path, Integer defaultValue) {
		try {
			return this.ctx.read(path, Integer.class);
		} catch (Exception e) {
			return defaultValue;
		}
	}

	public Float readFloat(String path, Float defaultValue) {
		try {
			return this.ctx.read(path, Float.class);
		} catch (Exception e) {
			return defaultValue;
		}
	}

	public Long readLong(String path, Long defaultValue) {
		try {
			return this.ctx.read(path, Long.class);
		} catch (Exception e) {
			return defaultValue;
		}
	}

	public Double readDouble(String path) {
		return this.ctx.read(path, Double.class);
	}

	public Integer readInteger(String path) {
		return this.ctx.read(path, Integer.class);
	}

	public Float readFloat(String path) {
		return this.ctx.read(path, Float.class);
	}

	public Long readLong(String path) {
		return this.ctx.read(path, Long.class);
	}

	public <T> T readNotNull(String path) {
		return this.ctx.read(path);
	}

	public void delete(String path) {
		this.ctx.delete(path);
	}

	public void set(String path, Object value) {
		this.ctx.set(path, value);
	}

	public String put(String path, String key, Object value) {
		this.ctx = this.ctx.put(path, key, value);
		return this.ctx.jsonString();
	}

	public String getString() {
		return this.ctx.jsonString();
	}

}
