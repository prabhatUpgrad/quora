package com.upgrad.sdproject.sdproject.utils.exceptions;

/**
 * @author prabhat on 13/04/18
 */
public class JsonParsingException extends RuntimeException {

	private Exception exception;

	public JsonParsingException(Exception e) {
		this.exception = e;
	}

	/**
	 * @return the exception
	 */
	public Exception getException() {
		return exception;
	}

	/**
	 * @param exception the exception to set
	 */
	public void setException(Exception exception) {
		this.exception = exception;
	}
}
