package com.upgrad.sdproject.sdproject.utils.exceptions;

import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * @author prabhat on 13/04/18
 */
@Aspect
@Order(1)
@Configuration
@Slf4j
public class GlobalExceptionHandler {

	@Pointcut("execution(public * com.upgrad.sdproject.sdproject.web..*Controller.*(..))")
	public void apiExceptionPointcut() {
	}

	@Around("apiExceptionPointcut()")
	public Object handleException(ProceedingJoinPoint pjp) throws Throwable {
		try {
			return pjp.proceed();
		} catch (NotFoundException e) {
			log.error(e.getMessage(), e);
			return new ResponseEntity<>(e, HttpStatus.NOT_FOUND);
		} catch (BadRequestException | IllegalArgumentException | UnauthorizedActionException e) {
			log.error(e.getMessage(), e);
			return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
