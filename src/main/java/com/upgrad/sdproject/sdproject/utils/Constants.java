package com.upgrad.sdproject.sdproject.utils;

/**
 * @author prabhat on 01/04/18
 */
public final class Constants {

	public static final class LogstashConstants {
		public static final String EMAIL = "email";
		public static final String USER_ID = "userId";
		public static final String ROLE = "role";
		public static final String COURSE_ID = "courseId";
		public static final String SESSION_ID = "sessionId";
		public static final String REQUEST_ID = "requestId";
		public static final String DISTINCT_ID = "distinctId";
		public static final String IP_ADDRESS = "ipAddress";
		public static final String HEADER_CLIENT_IP = "X-Forwarded-For";
		public static final String PLATFORM = "platform";
		public static final String HEADER_USER_AGENT = "User-Agent";
		public static final String DEVICE_TYPE = "deviceType";
		public static final String OPERATING_SYSTEM = "operatingSystem";
		public static final String BROWSER_INFO = "browserInfo";
		public static final String EVENT = "event";

		public static final String DEFAULT_STRING = "ANONYMOUS";
		public static final String DEFAULT_EMAIL = DEFAULT_STRING + "@" + DEFAULT_STRING;
		public static final String DEFAULT_NUMBER = "-1";

		private LogstashConstants() {
		}
	}

	public static final class ErrorConstants {
		public static final String JSON_CONVERSION_ERROR = "Error converting to json";
		public static final String USER_PROFILE_NOT_FOUND = "No UserProfile found";
		public static final String CATEGORY_NOT_FOUND = "No Category found";
		public static final String ACTION_NOT_ALLOWED = "Not Authorized to perform this action";
		public static final String CATEGORY_ALREADY_EXISTS = "Category already exists";
		public static final String QUESTION_NOT_FOUND = "No Question found";
		public static final String USER_NOT_FOUND = "No User found";
		public static final String QUESTION_ALREADY_EXISTS = "Question already exists";
		public static final String ACTION_NOT_FOUND = "Action not found";
		public static final String ANSWER_NOT_FOUND = "Answer not found";
		public static final String COMMENT_NOT_FOUND = "Comment not found";

		private ErrorConstants() {
		}
	}

	public static final class PaginationConstants {
		public static final String DEFAULT_PAGE_NUMBER = "0";
		public static final String DEFAULT_PAGE_SIZE = "20";
		public static final String DEFAULT_SORT_PARAM = "creationDate,desc";

		private PaginationConstants() {
		}
	}

	public static final class ApiConstants {
		public static final String RESPONSE_HEADER = "response";

		private ApiConstants() {
		}
	}
}
