package com.upgrad.sdproject.sdproject.utils.annotations;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static com.upgrad.sdproject.sdproject.utils.Constants.PaginationConstants.DEFAULT_PAGE_NUMBER;
import static com.upgrad.sdproject.sdproject.utils.Constants.PaginationConstants.DEFAULT_PAGE_SIZE;
import static com.upgrad.sdproject.sdproject.utils.Constants.PaginationConstants.DEFAULT_SORT_PARAM;

/**
 * A custom wrapper annotation to club the Swagger pagination related annotations. To avoid writing the boilerplate in every API
 *
 * @author prabhat on 16/04/18
 */
@ApiImplicitParams({
		@ApiImplicitParam(name = "page", value = "Page number", defaultValue = DEFAULT_PAGE_NUMBER, paramType = "query", dataType = "string"),
		@ApiImplicitParam(name = "size", value = "Page size", defaultValue = DEFAULT_PAGE_SIZE, paramType = "query", dataType = "string"),
		@ApiImplicitParam(name = "sort", value = "Sorting order", defaultValue = DEFAULT_SORT_PARAM, paramType = "query", dataType = "string")
})
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface PaginatedApi {
}
