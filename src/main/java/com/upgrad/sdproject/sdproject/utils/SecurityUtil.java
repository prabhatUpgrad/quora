package com.upgrad.sdproject.sdproject.utils;

import com.upgrad.sdproject.sdproject.web.entities.User;
import com.upgrad.sdproject.sdproject.web.services.userDetails.UpGradUserDetails;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Optional;

/**
 * @author prabhat on 12/04/18
 */
public class SecurityUtil {

	private SecurityUtil() {
	}

	public static User getUser() {
		if ((SecurityContextHolder.getContext().getAuthentication() == null)
				|| !(SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof UserDetails)) {
			return null;
		}
		UpGradUserDetails userDetails = (UpGradUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return userDetails.getUser();
	}
}
