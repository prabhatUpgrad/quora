package com.upgrad.sdproject.sdproject.utils.exceptions;

/**
 * @author prabhat on 13/04/18
 */
public class UnauthorizedActionException extends RuntimeException {

	public UnauthorizedActionException() {
	}

	public UnauthorizedActionException(String message) {
		super(message);
	}

	public UnauthorizedActionException(String message, Throwable cause) {
		super(message, cause);
	}

	public UnauthorizedActionException(Throwable cause) {
		super(cause);
	}

	public UnauthorizedActionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
