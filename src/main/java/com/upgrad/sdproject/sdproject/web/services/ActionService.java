package com.upgrad.sdproject.sdproject.web.services;

import com.upgrad.sdproject.sdproject.utils.Constants;
import com.upgrad.sdproject.sdproject.utils.exceptions.BadRequestException;
import com.upgrad.sdproject.sdproject.web.entities.Action;
import com.upgrad.sdproject.sdproject.web.entities.UserActivity;
import com.upgrad.sdproject.sdproject.web.repositories.ActionRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author prabhat on 10/06/18
 */
@Service
public class ActionService {

	private final ActionRepository actionRepository;

	private final ApplicationContext applicationContext;

	private final UserActivityService userActivityService;

	@Autowired public ActionService(ActionRepository actionRepository, ApplicationContext applicationContext,
			UserActivityService userActivityService) {
		this.actionRepository = actionRepository;
		this.applicationContext = applicationContext;
		this.userActivityService = userActivityService;
	}

	public Action recordNewAction(Action action) throws NotFoundException {
		validateAction(action);
		Action newAction = actionRepository.save(action);
		UserActivity.ActivityType activityType = newAction.getActionType().equals(Action.ActionType.LIKE) ?
				UserActivity.ActivityType.LIKE :
				UserActivity.ActivityType.FOLLOW;
		userActivityService.postActivity(activityType, newAction.getTargetEntityType(), newAction.getTargetId());
		return newAction;
	}

	private void validateAction(Action action) throws NotFoundException {
		validateTargetEntity(action.getTargetEntityType(), action.getTargetId());
	}

	private void validateTargetEntity(Action.TargetEntityType targetEntityType, Long targetId) throws NotFoundException {
		switch (targetEntityType) {
			case QUESTION:
				QuestionService questionService = applicationContext.getBean(QuestionService.class);
				if (!questionService.getQuestionById(targetId).isPresent()) {
					throw new NotFoundException(Constants.ErrorConstants.QUESTION_NOT_FOUND);
				}
				break;
			case ANSWER:
				AnswerService answerService = applicationContext.getBean(AnswerService.class);
				if (!answerService.getAnswerById(targetId).isPresent()) {
					throw new NotFoundException(Constants.ErrorConstants.ANSWER_NOT_FOUND);
				}
				break;
			case COMMENT:
				CommentService commentService = applicationContext.getBean(CommentService.class);
				if (!commentService.getCommentById(targetId).isPresent()) {
					throw new NotFoundException(Constants.ErrorConstants.COMMENT_NOT_FOUND);
				}
				break;
			default:
				throw new BadRequestException("Invalid Target type");
		}
	}

	public Optional<Action> updateAction(Long id, boolean status) {
		Optional<Action> existingAction = actionRepository.findById(id);
		if (existingAction.isPresent()) {
			Action action = existingAction.get();
			action.setSet(status);
			Action updatedAction = actionRepository.save(action);

			//Very naive way to figure this out. This can be majorly improved.
			UserActivity.ActivityType activityType = status ? UserActivity.ActivityType.LIKE : UserActivity.ActivityType.UNLIKE;
			userActivityService.postActivity(activityType, updatedAction.getTargetEntityType(), updatedAction.getTargetId());
			return Optional.of(updatedAction);
		}
		return Optional.empty();
	}
}
