package com.upgrad.sdproject.sdproject.web.services.userDetails;

import com.upgrad.sdproject.sdproject.web.entities.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;

/**
 * @author prabhat on 31/03/18
 */
@Component
public class UpGradUserDetails implements UserDetails {

	private static final long serialVersionUID = -5591695825246651801L;

	private transient User user;
	private transient List<GrantedAuthority> authorities;

	public UpGradUserDetails() {
	}

	public UpGradUserDetails(User user, List<GrantedAuthority> grantedAuthorities) {
		this.user = user;
		this.authorities = grantedAuthorities;
	}

	@Override public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override public String getPassword() {
		return user.getPassword();
	}

	@Override public String getUsername() {
		return user.getUserName();
	}

	@Override public boolean isAccountNonExpired() {
		return true;
	}

	@Override public boolean isAccountNonLocked() {
		return true;
	}

	@Override public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override public boolean isEnabled() {
		return true;
	}
}
