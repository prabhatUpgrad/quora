package com.upgrad.sdproject.sdproject.web.entities;

/**
 * @author prabhat on 11/04/18
 */

/**
 * Obvious meanings for Gender. DND stands for "Did Not Disclose"
 */
public enum Gender {
	MALE, FEMALE, TRANS, DND
}
