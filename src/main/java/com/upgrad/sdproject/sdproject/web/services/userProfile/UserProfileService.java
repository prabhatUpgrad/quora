package com.upgrad.sdproject.sdproject.web.services.userProfile;

import com.upgrad.sdproject.sdproject.web.entities.User;
import com.upgrad.sdproject.sdproject.web.entities.UserProfile;

import java.util.Optional;

/**
 * @author prabhat on 12/04/18
 */
public interface UserProfileService {
	Optional<UserProfile> getUserProfileByUser(User user);

	Optional<UserProfile> getUserProfileById(Long id);

	Optional<UserProfile> updateUserProfile(Long id, UserProfile updatedProfile);

	Optional<UserProfile> patchUserProfile(Long id, String json);
}
