package com.upgrad.sdproject.sdproject.web.services;

import com.upgrad.sdproject.sdproject.utils.JsonUtil;
import com.upgrad.sdproject.sdproject.utils.SecurityUtil;
import com.upgrad.sdproject.sdproject.web.entities.Action;
import com.upgrad.sdproject.sdproject.web.entities.Answer;
import com.upgrad.sdproject.sdproject.web.entities.Comment;
import com.upgrad.sdproject.sdproject.web.entities.User;
import com.upgrad.sdproject.sdproject.web.entities.UserActivity;
import com.upgrad.sdproject.sdproject.web.repositories.AnswerRepository;
import com.upgrad.sdproject.sdproject.web.repositories.CommentsRepository;
import com.upgrad.sdproject.sdproject.web.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

/**
 * @author prabhat on 10/06/18
 */
@Service
public class CommentService {

	private final CommentsRepository commentsRepository;

	private final AnswerRepository answerRepository;

	private final UserRepository userRepository;
	private final UserActivityService userActivityService;

	@Autowired public CommentService(CommentsRepository commentsRepository, AnswerRepository answerRepository, UserRepository userRepository,
			UserActivityService userActivityService) {
		this.commentsRepository = commentsRepository;
		this.answerRepository = answerRepository;
		this.userRepository = userRepository;
		this.userActivityService = userActivityService;
	}

	public Optional<Comment> getCommentById(long id) {
		return commentsRepository.findById(id);
	}

	public Page<Comment> getCommentsByAnswers(long answerId, Pageable pageable) {
		Answer answer = answerRepository.findById(answerId).orElseThrow(EntityNotFoundException::new);
		return commentsRepository.findAllByAnswer(answer, pageable);
	}

	public Page<Comment> getCommentsByUser(long userId, Pageable pageable) {
		User user = userRepository.findById(userId).orElseThrow(EntityNotFoundException::new);
		return commentsRepository.findAllByUser(user, pageable);
	}

	public Optional<Comment> createNewComment(Comment comment) {
		if (commentsRepository.exists(Example.of(comment))) {
			return Optional.empty();
		}
		Answer answer = answerRepository.findById(comment.getAnswer().getId()).orElseThrow(EntityNotFoundException::new);
		comment.setAnswer(answer);
		comment.setUser(SecurityUtil.getUser());
		Comment newComment = commentsRepository.saveAndFlush(comment);
		userActivityService.postActivity(UserActivity.ActivityType.POST_COMMENT, Action.TargetEntityType.COMMENT, newComment.getId());
		return Optional.of(newComment);
	}

	public Optional<Comment> patchComment(long id, String updatedJson) {
		Optional<Comment> existingComment = commentsRepository.findById(id);
		if (existingComment.isPresent()) {
			Comment comment = existingComment.get();
			String patchJson = JsonUtil.patchJson(JsonUtil.toJson(comment), updatedJson);
			Comment updatedComment = JsonUtil.fromJson(patchJson, Comment.class);
			return Optional.of(commentsRepository.save(updatedComment));
		}
		return Optional.empty();
	}
}
