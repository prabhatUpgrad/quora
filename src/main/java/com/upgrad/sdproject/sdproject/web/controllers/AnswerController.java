package com.upgrad.sdproject.sdproject.web.controllers;

import com.upgrad.sdproject.sdproject.utils.annotations.PaginatedApi;
import com.upgrad.sdproject.sdproject.web.entities.Answer;
import com.upgrad.sdproject.sdproject.web.services.AnswerService;
import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Optional;

import static com.upgrad.sdproject.sdproject.utils.Constants.ApiConstants.RESPONSE_HEADER;
import static com.upgrad.sdproject.sdproject.utils.Constants.ErrorConstants.ANSWER_NOT_FOUND;

/**
 * @author prabhat on 10/06/18
 */
@Controller
@RequestMapping("/answer")
@Slf4j
public class AnswerController {

	private final AnswerService answerService;

	@Autowired public AnswerController(AnswerService answerService) {
		this.answerService = answerService;
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> getAnswerById(@PathVariable("id") long id) {
		Optional<Answer> answer = answerService.getAnswerById(id);
		return answer.<ResponseEntity<?>>map(ResponseEntity::ok)
				.orElseGet(() -> new ResponseEntity<>(ANSWER_NOT_FOUND, HttpStatus.NOT_FOUND));
	}

	@GetMapping("/question/{id}")
	@PaginatedApi
	public ResponseEntity<Page<Answer>> getAnswersByQuestions(@PathVariable("id") long questionId, Pageable pageable) {
		return ResponseEntity.ok(answerService.getAnswersByQuestions(questionId, pageable));
	}

	@GetMapping("/user/{id}")
	@PaginatedApi
	public ResponseEntity<Page<Answer>> getAnswersByUser(@PathVariable("id") long userId, Pageable pageable) throws NotFoundException {
		return ResponseEntity.ok(answerService.getAnswersByUser(userId, pageable));
	}

	@PostMapping
	public ResponseEntity<?> createNewAnswer(@RequestBody Answer answer) {
		Optional<Answer> newAnswer = answerService.createNewAnswer(answer);
		return newAnswer.map(result -> {
			URI location = ServletUriComponentsBuilder
					.fromCurrentRequest().path("/{id}")
					.buildAndExpand(result.getId()).toUri();
			return ResponseEntity.created(location).build();
		}).orElse(ResponseEntity.noContent().header(RESPONSE_HEADER, "Answer could not be submitted").build());
	}

	@PatchMapping("/{id}")
	public ResponseEntity<?> patchAnswerById(@PathVariable("id") long id, @RequestParam("patchJson") String patchJson) {
		Optional<Answer> updatedAnswer = answerService.patchAnswer(id, patchJson);
		return updatedAnswer.<ResponseEntity<?>>map(updatedAnswer1 -> new ResponseEntity<>(updatedAnswer1, HttpStatus.OK))
				.orElseGet(() -> new ResponseEntity<>(ANSWER_NOT_FOUND, HttpStatus.NOT_FOUND));
	}
}
