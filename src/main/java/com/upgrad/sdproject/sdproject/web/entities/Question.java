package com.upgrad.sdproject.sdproject.web.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.JoinFormula;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * @author prabhat on 14/04/18
 */
@Entity
@Data
@EqualsAndHashCode(callSuper=false)
public class Question extends Auditable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotNull
	@Column(nullable = false, unique = true, length = 150)
	private String title;

	@NotNull
	@Column(columnDefinition = "TEXT not null")
	private String content;

	@ManyToOne
	@OnDelete(action = OnDeleteAction.CASCADE)
	private User user;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "question_category_mapping", joinColumns = @JoinColumn(name = "question_id"), inverseJoinColumns = @JoinColumn(name = "category_id"))
	private Set<Category> categories;

	@Formula("(select count(*) from action a where a.target_entity_type=0 and a.action_type=0 and a.is_set=true and a.target_id=question_0.id)")
	private int likes;
}
