package com.upgrad.sdproject.sdproject.web.controllers;

import com.upgrad.sdproject.sdproject.web.services.registration.RegistrationService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author prabhat on 01/04/18
 */
@RestController
public class RegistrationController {

	private static final String HEADER_SESSION_ID = "sessionID";

	private final RegistrationService registrationService;

	@Autowired public RegistrationController(RegistrationService registrationService) {
		this.registrationService = registrationService;
	}

	@PostMapping("/signup")
	@ResponseBody
	@ApiOperation(value = "Endpoint to Create a new account on UpGrad-Quora.", notes = "<pre>"
			+ "{\n" +
			"	\"firstName\": \"FirstName\",\n" +
			"	\"lastName\": \"LastName\",\n" +
			"	\"userName\": \"user@upgrad.com\",\n" +
			"	\"email\": \"user@upgrad.com\",\n" +
			"	\"password\": \"password\",\n" +
			"	\"userProfile\":{ " +
			"		\"aboutMe\": \"I am the wall\", "
			+ "		\"dateOfBirth\":\"1975-02-20\", "
			+ "		\"gender\":0,"
			+ "		\"contactNumber\":null,"
			+ "		\"country\":\"India\""
			+ "		}" +
			"}\n" +
			"</pre> ")
	public String signUp(@RequestBody String json) throws IOException {
		return registrationService.signUp(json);
	}

	@PostMapping(value = "/login")
	@ResponseBody
	@ApiOperation(value = "Login Request", notes = "Login Request for a user. It requires the following json : "
			+ "<pre>"
			+ "{\n" +
			"	\"username\": \"test.user@upgrad.com\",\n" +
			"	\"password\": \"password\"\n" +
			"}"
			+ "</pre>")
	@ApiImplicitParams({ @ApiImplicitParam(name = "Request Body", value = "Json", required = true, dataType = "String", paramType = "body"),
			@ApiImplicitParam(name = HEADER_SESSION_ID, value = "String", required = true, dataType = "String", paramType = "header") })
	public ResponseEntity<?> login(HttpServletRequest request) {
		return registrationService.login(request);
	}
}
