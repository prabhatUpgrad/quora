package com.upgrad.sdproject.sdproject.web.repositories;

import com.upgrad.sdproject.sdproject.web.entities.Answer;
import com.upgrad.sdproject.sdproject.web.entities.Question;
import com.upgrad.sdproject.sdproject.web.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author prabhat on 10/06/18
 */
@Repository
public interface AnswerRepository extends JpaRepository<Answer, Long> {
	Page<Answer> findAllByQuestion(Question question, Pageable pageable);

	Page<Answer> findAllByUser(User user, Pageable pageable);
}
