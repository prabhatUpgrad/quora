package com.upgrad.sdproject.sdproject.web.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author prabhat on 10/03/18
 */
@RestController
public class HelloController {

	@GetMapping(value = { "", "/", "/home" })
	public String helloWorld() {
		return "Hello World!";
	}
}
