package com.upgrad.sdproject.sdproject.web.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 * @author prabhat on 10/06/18
 */
@Entity
@Data
@EqualsAndHashCode(callSuper=false)
public class Comment extends Auditable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotNull
	@Column(columnDefinition = "TEXT not null")
	private String content;

	@ManyToOne
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Answer answer;

	@ManyToOne
	@OnDelete(action = OnDeleteAction.CASCADE)
	private User user;
}
