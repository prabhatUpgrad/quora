package com.upgrad.sdproject.sdproject.web.repositories;

import com.upgrad.sdproject.sdproject.web.entities.User;
import com.upgrad.sdproject.sdproject.web.entities.UserProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author prabhat on 12/04/18
 */
@Repository
public interface UserProfileRepository extends JpaRepository<UserProfile, Long>{

	Optional<UserProfile> findByUser(User user);
}
