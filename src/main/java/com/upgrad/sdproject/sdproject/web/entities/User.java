package com.upgrad.sdproject.sdproject.web.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * @author prabhat on 10/03/18
 */
@Entity
@Table(name = "\"user\"") //We need escaping since `User` is a reserved keyword in PostGres.
@Data
@EqualsAndHashCode(callSuper=false)
public class User extends Auditable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotNull
	@Column(nullable = false)
	private String firstName;

	private String lastName;

	@NotNull
	@Column(nullable = false, unique = true, length = 125)
	private String userName;

	@NotNull
	@Column(nullable = false, unique = true, length = 125)
	private String email;

	@NotNull
	@Column(nullable = false)
	private String password;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "user")
	private UserProfile userProfile;

	@Version
	@Column(nullable = false, columnDefinition = "int default 0")
	private int version;

	public boolean hasRole(String role) {
		for (Role queryRole : getRoles()) {
			if ((role).equals(queryRole.getRoleName())) {
				return true;
			}
		}
		return false;
	}
}
