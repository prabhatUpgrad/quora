package com.upgrad.sdproject.sdproject.web.controllers;

import com.upgrad.sdproject.sdproject.utils.annotations.PaginatedApi;
import com.upgrad.sdproject.sdproject.web.services.UserActivityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author prabhat on 10/06/18
 */
@Controller
@RequestMapping("/activity")
@Slf4j
public class UserActivityController {

	private final UserActivityService userActivityService;

	@Autowired public UserActivityController(UserActivityService userActivityService) {
		this.userActivityService = userActivityService;
	}

	@GetMapping
	@PaginatedApi
	public ResponseEntity<?> getAllActivities(Pageable pageable) {
		return ResponseEntity.ok(userActivityService.getAllActivities(pageable));
	}

	@GetMapping("/user/{id}")
	@PaginatedApi
	public ResponseEntity<?> getAllActivitiesForAUser(@PathVariable("id") Long userId, Pageable pageable) {
		return ResponseEntity.ok(userActivityService.getAllActivitiesForUser(userId, pageable));
	}
}
