package com.upgrad.sdproject.sdproject.web.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * @author prabhat on 10/06/18
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class UserActivity extends Auditable {

	public enum ActivityType {
		LIKE, UNLIKE, POST_COMMENT, POST_ANSWER, POST_QUESTION, FOLLOW, UNFOLLOW
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Enumerated
	private ActivityType activityType;

	@Enumerated
	private Action.TargetEntityType targetEntityType;

	private Long targetId;

	@ManyToOne
	@OnDelete(action = OnDeleteAction.CASCADE)
	private User user;
}
