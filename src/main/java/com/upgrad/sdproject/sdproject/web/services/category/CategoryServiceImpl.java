package com.upgrad.sdproject.sdproject.web.services.category;

import com.upgrad.sdproject.sdproject.utils.JsonUtil;
import com.upgrad.sdproject.sdproject.web.entities.Category;
import com.upgrad.sdproject.sdproject.web.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.function.Function;

/**
 * @author prabhat on 14/04/18
 */
@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

	private final CategoryRepository categoryRepository;

	@Autowired public CategoryServiceImpl(CategoryRepository categoryRepository) {
		this.categoryRepository = categoryRepository;
	}

	@Override public Optional<Category> getCategoryById(long id) {
		return categoryRepository.findById(id);
	}

	@Override public Optional<Category> getCategoryByName(String name) {
		return categoryRepository.findByName(name);
	}

	@Override public Page<Category> findAllCategories(Pageable pageable) {
		return categoryRepository.findAll(pageable);
	}

	@Override public Optional<Category> createNewCategory(Category category) {
		if (categoryRepository.exists(Example.of(category))) {
			return Optional.empty();
		}
		return Optional.of(categoryRepository.saveAndFlush(category));
	}

	@Override public Optional<Category> updateCategory(long id, Category category) {
		Function<Category, Category> function = existingCategory -> {
			existingCategory.setName(category.getName());
			existingCategory.setDescription(category.getDescription());
			return existingCategory;
		};
		return editCategory(id, function);
	}

	@Override public Optional<Category> patchCategory(long id, String updatedJson) {
		Function<Category, Category> function = existingCategory -> {
			String patchJson = JsonUtil.patchJson(JsonUtil.toJson(existingCategory), updatedJson);
			return JsonUtil.fromJson(patchJson, Category.class);
		};
		return editCategory(id, function);
	}

	private Optional<Category> editCategory(Long id, Function<Category, Category> function) {
		Optional<Category> existingCategory = categoryRepository.findById(id);
		if (existingCategory.isPresent()) {
			Category category = existingCategory.get();
			category = function.apply(category);
			return Optional.of(categoryRepository.save(category));
		}
		return Optional.empty();
	}
}
