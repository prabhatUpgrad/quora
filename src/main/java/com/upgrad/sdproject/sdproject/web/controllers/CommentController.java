package com.upgrad.sdproject.sdproject.web.controllers;

import com.upgrad.sdproject.sdproject.utils.annotations.PaginatedApi;
import com.upgrad.sdproject.sdproject.web.entities.Comment;
import com.upgrad.sdproject.sdproject.web.services.CommentService;
import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Optional;

import static com.upgrad.sdproject.sdproject.utils.Constants.ApiConstants.RESPONSE_HEADER;
import static com.upgrad.sdproject.sdproject.utils.Constants.ErrorConstants.COMMENT_NOT_FOUND;

/**
 * @author prabhat on 10/06/18
 */
@Controller
@RequestMapping("/comment")
@Slf4j
public class CommentController {

	private final CommentService commentService;

	@Autowired public CommentController(CommentService commentService) {
		this.commentService = commentService;
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> getCommentById(@PathVariable("id") long id) {
		Optional<Comment> comment = commentService.getCommentById(id);
		return comment.<ResponseEntity<?>>map(ResponseEntity::ok)
				.orElseGet(() -> new ResponseEntity<>(COMMENT_NOT_FOUND, HttpStatus.NOT_FOUND));
	}

	@GetMapping("/answer/{id}")
	@PaginatedApi
	public ResponseEntity<Page<Comment>> getCommentsByAnswers(@PathVariable("id") long answerId, Pageable pageable) {
		return ResponseEntity.ok(commentService.getCommentsByAnswers(answerId, pageable));
	}

	@GetMapping("/user/{id}")
	@PaginatedApi
	public ResponseEntity<Page<Comment>> getCommentsByUser(@PathVariable("id") long userId, Pageable pageable) throws NotFoundException {
		return ResponseEntity.ok(commentService.getCommentsByUser(userId, pageable));
	}

	@PostMapping
	public ResponseEntity<?> createNewAnswer(@RequestBody Comment comment) {
		Optional<Comment> newComment = commentService.createNewComment(comment);
		return newComment.map(result -> {
			URI location = ServletUriComponentsBuilder
					.fromCurrentRequest().path("/{id}")
					.buildAndExpand(result.getId()).toUri();
			return ResponseEntity.created(location).build();
		}).orElse(ResponseEntity.noContent().header(RESPONSE_HEADER, "Comment could not be submitted").build());
	}

	@PatchMapping("/{id}")
	public ResponseEntity<?> patchCommentById(@PathVariable("id") long id, @RequestParam("patchJson") String patchJson) {
		Optional<Comment> updatedComment = commentService.patchComment(id, patchJson);
		return updatedComment.<ResponseEntity<?>>map(updatedComment1 -> new ResponseEntity<>(updatedComment1, HttpStatus.OK))
				.orElseGet(() -> new ResponseEntity<>(COMMENT_NOT_FOUND, HttpStatus.NOT_FOUND));
	}
}
