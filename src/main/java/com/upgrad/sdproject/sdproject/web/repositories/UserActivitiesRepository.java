package com.upgrad.sdproject.sdproject.web.repositories;

import com.upgrad.sdproject.sdproject.web.entities.User;
import com.upgrad.sdproject.sdproject.web.entities.UserActivity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author prabhat on 10/06/18
 */
@Repository
public interface UserActivitiesRepository extends JpaRepository<UserActivity, Long> {
	Page<UserActivity> findAllByUser(User user, Pageable pageable);
}
