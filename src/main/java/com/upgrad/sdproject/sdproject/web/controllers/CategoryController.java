package com.upgrad.sdproject.sdproject.web.controllers;

import com.upgrad.sdproject.sdproject.utils.annotations.PaginatedApi;
import com.upgrad.sdproject.sdproject.web.entities.Category;
import com.upgrad.sdproject.sdproject.web.services.category.CategoryService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Optional;

import static com.upgrad.sdproject.sdproject.utils.Constants.ApiConstants.RESPONSE_HEADER;
import static com.upgrad.sdproject.sdproject.utils.Constants.ErrorConstants.CATEGORY_ALREADY_EXISTS;
import static com.upgrad.sdproject.sdproject.utils.Constants.ErrorConstants.CATEGORY_NOT_FOUND;
import static com.upgrad.sdproject.sdproject.utils.Constants.PaginationConstants.DEFAULT_PAGE_NUMBER;
import static com.upgrad.sdproject.sdproject.utils.Constants.PaginationConstants.DEFAULT_PAGE_SIZE;
import static com.upgrad.sdproject.sdproject.utils.Constants.PaginationConstants.DEFAULT_SORT_PARAM;

/**
 * @author prabhat on 14/04/18
 */
@Controller
@Slf4j
@RequestMapping("/category")
public class CategoryController {

	private final CategoryService categoryService;

	@Autowired public CategoryController(CategoryService categoryService) {
		this.categoryService = categoryService;
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> getCategoryById(@PathVariable("id") long id) {
		Optional<Category> category = categoryService.getCategoryById(id);
		return category.<ResponseEntity<?>>map(ResponseEntity::ok)
				.orElseGet(() -> new ResponseEntity<>(CATEGORY_NOT_FOUND, HttpStatus.NOT_FOUND));
	}

	@GetMapping("/{name}")
	public ResponseEntity<?> getCategoryByName(@PathVariable("name") String name) {
		Optional<Category> category = categoryService.getCategoryByName(name);
		return category.<ResponseEntity<?>>map(ResponseEntity::ok)
				.orElseGet(() -> new ResponseEntity<>(CATEGORY_NOT_FOUND, HttpStatus.NOT_FOUND));
	}

	@GetMapping("/all")
	@PaginatedApi
	public ResponseEntity<Page<Category>> getAllCategories(Pageable pageable) {
		return ResponseEntity.ok(categoryService.findAllCategories(pageable));
	}

	@PostMapping
	public ResponseEntity<?> createNewCategory(@RequestBody Category category) {
		Optional<Category> resultCategory = categoryService.createNewCategory(category);
		return resultCategory.map(result -> {
			URI location = ServletUriComponentsBuilder
					.fromCurrentRequest().path("/{id}")
					.buildAndExpand(result.getId()).toUri();
			return ResponseEntity.created(location).build();
		}).orElse(ResponseEntity.noContent().header(RESPONSE_HEADER, CATEGORY_ALREADY_EXISTS).build());
	}

	@PutMapping("/{id")
	public ResponseEntity<?> updateCategoryById(@PathVariable("id") long id, @RequestBody Category category) {
		Optional<Category> updatedCategory = categoryService.updateCategory(id, category);
		return updatedCategory.<ResponseEntity<?>>map(updatedCategory1 -> new ResponseEntity<>(updatedCategory1, HttpStatus.OK))
				.orElseGet(() -> new ResponseEntity<>(CATEGORY_NOT_FOUND, HttpStatus.NOT_FOUND));
	}

	@PatchMapping("/{id")
	public ResponseEntity<?> patchCategoryById(@PathVariable("id") long id, @RequestParam("patchJson") String patchJson) {
		Optional<Category> updatedCategory = categoryService.patchCategory(id, patchJson);
		return updatedCategory.<ResponseEntity<?>>map(updatedCategory1 -> new ResponseEntity<>(updatedCategory1, HttpStatus.OK))
				.orElseGet(() -> new ResponseEntity<>(CATEGORY_NOT_FOUND, HttpStatus.NOT_FOUND));
	}
}
