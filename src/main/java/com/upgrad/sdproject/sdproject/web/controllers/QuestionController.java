package com.upgrad.sdproject.sdproject.web.controllers;

import com.upgrad.sdproject.sdproject.utils.annotations.PaginatedApi;
import com.upgrad.sdproject.sdproject.web.entities.Question;
import com.upgrad.sdproject.sdproject.web.services.QuestionService;
import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import static com.upgrad.sdproject.sdproject.utils.Constants.ApiConstants.RESPONSE_HEADER;
import static com.upgrad.sdproject.sdproject.utils.Constants.ErrorConstants.QUESTION_ALREADY_EXISTS;
import static com.upgrad.sdproject.sdproject.utils.Constants.ErrorConstants.QUESTION_NOT_FOUND;

/**
 * @author prabhat on 16/04/18
 */
@Controller
@RequestMapping("/question")
@Slf4j
public class QuestionController {

	private final QuestionService questionService;

	@Autowired public QuestionController(QuestionService questionService) {
		this.questionService = questionService;
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> getQuestionById(@PathVariable("id") long id) {
		Optional<Question> question = questionService.getQuestionById(id);
		return question.<ResponseEntity<?>>map(ResponseEntity::ok)
				.orElseGet(() -> new ResponseEntity<>(QUESTION_NOT_FOUND, HttpStatus.NOT_FOUND));
	}

	@GetMapping("/category")
	@PaginatedApi
	public ResponseEntity<Page<Question>> getQuestionsByCategories(@RequestParam("categories") List<String> categories, Pageable pageable) {
		return ResponseEntity.ok(questionService.getQuestionsByCategories(categories, pageable));
	}

	@GetMapping("/user/{id}")
	@PaginatedApi
	public ResponseEntity<Page<Question>> getQuestionsByUser(@PathVariable("id") long userId, Pageable pageable) throws NotFoundException {
		return ResponseEntity.ok(questionService.getQuestionsByUser(userId, pageable));
	}

	@PostMapping
	public ResponseEntity<?> createNewQuestion(@RequestBody Question question) {
		Optional<Question> newQuestion = questionService.createNewQuestion(question);
		return newQuestion.map(result -> {
			URI location = ServletUriComponentsBuilder
					.fromCurrentRequest().path("/{id}")
					.buildAndExpand(result.getId()).toUri();
			return ResponseEntity.created(location).build();
		}).orElse(ResponseEntity.noContent().header(RESPONSE_HEADER, QUESTION_ALREADY_EXISTS).build());
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> updateQuestionById(@PathVariable("id") long id, @RequestBody Question question) {
		Optional<Question> updatedQuestion = questionService.updateQuestion(id, question);
		return updatedQuestion.<ResponseEntity<?>>map(updatedQuestion1 -> new ResponseEntity<>(updatedQuestion1, HttpStatus.OK))
				.orElseGet(() -> new ResponseEntity<>(QUESTION_NOT_FOUND, HttpStatus.NOT_FOUND));
	}

	@PatchMapping("/{id}")
	public ResponseEntity<?> patchQuestionById(@PathVariable("id") long id, @RequestParam("patchJson") String patchJson) {
		Optional<Question> updatedQuestion = questionService.patchQuestion(id, patchJson);
		return updatedQuestion.<ResponseEntity<?>>map(updatedQuestion1 -> new ResponseEntity<>(updatedQuestion1, HttpStatus.OK))
				.orElseGet(() -> new ResponseEntity<>(QUESTION_NOT_FOUND, HttpStatus.NOT_FOUND));
	}
}
