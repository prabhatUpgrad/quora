package com.upgrad.sdproject.sdproject.web.services.userProfile;

import com.upgrad.sdproject.sdproject.utils.JsonUtil;
import com.upgrad.sdproject.sdproject.utils.SecurityUtil;
import com.upgrad.sdproject.sdproject.utils.exceptions.UnauthorizedActionException;
import com.upgrad.sdproject.sdproject.web.entities.User;
import com.upgrad.sdproject.sdproject.web.entities.UserProfile;
import com.upgrad.sdproject.sdproject.web.repositories.UserProfileRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.function.Function;

import static com.upgrad.sdproject.sdproject.utils.Constants.ErrorConstants.ACTION_NOT_ALLOWED;

/**
 * @author prabhat on 12/04/18
 */
@Service
@Transactional
@Slf4j
public class UserProfileServiceImpl implements UserProfileService {

	private final UserProfileRepository userProfileRepository;

	@Autowired public UserProfileServiceImpl(UserProfileRepository userProfileRepository) {
		this.userProfileRepository = userProfileRepository;
	}

	@Override public Optional<UserProfile> getUserProfileByUser(User user) {
		return userProfileRepository.findByUser(user);
	}

	@Override public Optional<UserProfile> getUserProfileById(Long id) {
		return userProfileRepository.findById(id);
	}

	@Override public Optional<UserProfile> updateUserProfile(Long id, UserProfile updatedProfile) {
		Function<UserProfile, UserProfile> function = existingProfile -> existingProfile.getUpdatedFromSource(updatedProfile);
		return editUserProfile(id, function);
	}

	@Override public Optional<UserProfile> patchUserProfile(Long id, String updatedJson) {
		Function<UserProfile, UserProfile> function = existingProfile -> {
			String patchJson = JsonUtil.patchJson(JsonUtil.toJson(existingProfile), updatedJson);
			UserProfile updatedUserProfile = JsonUtil.fromJson(patchJson, UserProfile.class);
			updatedUserProfile.setUser(existingProfile.getUser());
			return updatedUserProfile;
		};
		return editUserProfile(id, function);
	}

	private Optional<UserProfile> editUserProfile(Long id, Function<UserProfile, UserProfile> function) {
		if (!id.equals(SecurityUtil.getUser().getUserProfile().getId())) {
			throw new UnauthorizedActionException(ACTION_NOT_ALLOWED);
		}
		Optional<UserProfile> existingUserProfile = userProfileRepository.findById(id);
		if (existingUserProfile.isPresent()) {
			UserProfile userProfile = existingUserProfile.get();
			userProfile = function.apply(userProfile);
			return Optional.of(userProfileRepository.save(userProfile));
		}
		return Optional.empty();
	}
}
