package com.upgrad.sdproject.sdproject.web.repositories;

import com.upgrad.sdproject.sdproject.web.entities.SessionToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author prabhat on 31/03/18
 */
@Repository
public interface SessionTokenRepository extends JpaRepository<SessionToken, Long> {

	SessionToken findByToken(String token);
}
