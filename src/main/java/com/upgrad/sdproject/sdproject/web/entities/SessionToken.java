package com.upgrad.sdproject.sdproject.web.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import java.util.Calendar;
import java.util.Date;

/**
 * @author prabhat on 31/03/18
 */
@Entity
@Getter @Setter
public class SessionToken {

	private static final int EXPIRY = 60 * 24 * 30 * 3;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotNull
	@Column(nullable = false, unique = true)
	private String token;

	@ManyToOne
	private User user;

	private String lastSessionId;

	@NotNull
	@Column(updatable = false, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	private Date created = new Date();

	public SessionToken() {
	}

	public SessionToken(String token, User user) {
		this.user = user;
		this.token = token;
	}

	public boolean isExpired() {
		Date expiryDate = addMinutes(created, EXPIRY);
		return expiryDate.before(new Date());
	}

	//TODO : Replace this with "import org.apache.commons.lang3.time.DateUtils;"
	private Date addMinutes(Date date, int minutes) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MINUTE, minutes);
		return c.getTime();
	}
}
