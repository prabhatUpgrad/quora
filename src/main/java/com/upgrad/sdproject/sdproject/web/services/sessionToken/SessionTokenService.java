package com.upgrad.sdproject.sdproject.web.services.sessionToken;

import com.upgrad.sdproject.sdproject.web.entities.SessionToken;

/**
 * @author prabhat on 31/03/18
 */
public interface SessionTokenService {
	SessionToken updateSessionToken(SessionToken sessionToken);

	SessionToken findSessionTokensByTokenEquals(String token);

	void deleteSessionToken(SessionToken sessionToken);
}
