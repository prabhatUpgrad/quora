package com.upgrad.sdproject.sdproject.web.services.user;

import com.upgrad.sdproject.sdproject.web.entities.User;

/**
 * @author prabhat on 31/03/18
 */
public interface UserService {
	User findUserByEmail(String email);

	User findUserByUsername(String username);

	User saveUser(User user);
}
