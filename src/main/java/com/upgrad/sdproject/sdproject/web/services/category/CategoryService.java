package com.upgrad.sdproject.sdproject.web.services.category;

import com.upgrad.sdproject.sdproject.web.entities.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * @author prabhat on 14/04/18
 */
public interface CategoryService {
	Optional<Category> getCategoryById(long id);

	Optional<Category> getCategoryByName(String name);

	Page<Category> findAllCategories(Pageable pageable);

	Optional<Category> createNewCategory(Category category);

	Optional<Category> updateCategory(long id, Category category);

	Optional<Category> patchCategory(long id, String updatedJson);
}
