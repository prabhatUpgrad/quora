package com.upgrad.sdproject.sdproject.web.services;

import com.upgrad.sdproject.sdproject.utils.JsonUtil;
import com.upgrad.sdproject.sdproject.utils.SecurityUtil;
import com.upgrad.sdproject.sdproject.web.entities.Action;
import com.upgrad.sdproject.sdproject.web.entities.Answer;
import com.upgrad.sdproject.sdproject.web.entities.Question;
import com.upgrad.sdproject.sdproject.web.entities.User;
import com.upgrad.sdproject.sdproject.web.entities.UserActivity;
import com.upgrad.sdproject.sdproject.web.repositories.AnswerRepository;
import com.upgrad.sdproject.sdproject.web.repositories.QuestionRepository;
import com.upgrad.sdproject.sdproject.web.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

/**
 * @author prabhat on 10/06/18
 */
@Service
public class AnswerService {

	private final AnswerRepository answerRepository;

	private final QuestionRepository questionRepository;

	private final UserRepository userRepository;
	private final UserActivityService userActivityService;

	@Autowired public AnswerService(AnswerRepository answerRepository, QuestionRepository questionRepository, UserRepository userRepository,
			UserActivityService userActivityService) {
		this.answerRepository = answerRepository;
		this.questionRepository = questionRepository;
		this.userRepository = userRepository;
		this.userActivityService = userActivityService;
	}

	public Optional<Answer> getAnswerById(long id) {
		return answerRepository.findById(id);
	}

	public Page<Answer> getAnswersByQuestions(long questionId, Pageable pageable) {
		Question question = questionRepository.findById(questionId).orElseThrow(EntityNotFoundException::new);
		return answerRepository.findAllByQuestion(question, pageable);
	}

	public Page<Answer> getAnswersByUser(long userId, Pageable pageable) {
		User user = userRepository.findById(userId).orElseThrow(EntityNotFoundException::new);
		return answerRepository.findAllByUser(user, pageable);
	}

	public Optional<Answer> createNewAnswer(Answer answer) {
		if (answerRepository.exists(Example.of(answer))) {
			return Optional.empty();
		}
		Question question = questionRepository.findById(answer.getQuestion().getId()).orElseThrow(EntityNotFoundException::new);
		answer.setQuestion(question);
		answer.setUser(SecurityUtil.getUser());
		Answer newAnswer = answerRepository.saveAndFlush(answer);
		userActivityService.postActivity(UserActivity.ActivityType.POST_ANSWER, Action.TargetEntityType.ANSWER, newAnswer.getId());
		return Optional.of(newAnswer);
	}

	public Optional<Answer> patchAnswer(long id, String updatedJson) {
		Optional<Answer> existingAnswer = answerRepository.findById(id);
		if (existingAnswer.isPresent()) {
			Answer answer = existingAnswer.get();
			String patchJson = JsonUtil.patchJson(JsonUtil.toJson(existingAnswer), updatedJson);
			Answer updatedAnswer = JsonUtil.fromJson(patchJson, Answer.class);
			return Optional.of(answerRepository.save(answer));
		}
		return Optional.empty();
	}
}
