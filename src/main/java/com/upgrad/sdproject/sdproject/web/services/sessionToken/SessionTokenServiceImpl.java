package com.upgrad.sdproject.sdproject.web.services.sessionToken;

import com.upgrad.sdproject.sdproject.web.entities.SessionToken;
import com.upgrad.sdproject.sdproject.web.repositories.SessionTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author prabhat on 31/03/18
 */
@Service
@Transactional
public class SessionTokenServiceImpl implements SessionTokenService {

	private final SessionTokenRepository sessionTokenRepository;

	@Autowired public SessionTokenServiceImpl(SessionTokenRepository sessionTokenRepository) {
		this.sessionTokenRepository = sessionTokenRepository;
	}

	@Override
	public SessionToken updateSessionToken(SessionToken sessionToken) {
		return sessionTokenRepository.save(sessionToken);
	}

	@Override
	public SessionToken findSessionTokensByTokenEquals(String token) {
		return sessionTokenRepository.findByToken(token);
	}

	@Override public void deleteSessionToken(SessionToken sessionToken) {
		sessionTokenRepository.delete(sessionToken);
	}
}
