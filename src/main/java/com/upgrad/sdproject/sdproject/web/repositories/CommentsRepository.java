package com.upgrad.sdproject.sdproject.web.repositories;

import com.upgrad.sdproject.sdproject.web.entities.Answer;
import com.upgrad.sdproject.sdproject.web.entities.Comment;
import com.upgrad.sdproject.sdproject.web.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author prabhat on 10/06/18
 */
@Repository
public interface CommentsRepository extends JpaRepository<Comment, Long> {
	Page<Comment> findAllByAnswer(Answer answer, Pageable pageable);

	Page<Comment> findAllByUser(User user, Pageable pageable);
}
