package com.upgrad.sdproject.sdproject.web.controllers;

import com.upgrad.sdproject.sdproject.utils.Constants;
import com.upgrad.sdproject.sdproject.web.entities.Action;
import com.upgrad.sdproject.sdproject.web.entities.Question;
import com.upgrad.sdproject.sdproject.web.services.ActionService;
import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;

import static com.upgrad.sdproject.sdproject.utils.Constants.ErrorConstants.QUESTION_NOT_FOUND;

/**
 * @author prabhat on 10/06/18
 */
@Controller
@Slf4j
@RequestMapping("/action")
public class ActionController {

	private final ActionService actionService;

	@Autowired public ActionController(ActionService actionService) {
		this.actionService = actionService;
	}

	@PostMapping
	public ResponseEntity<?> recordNewAction(@RequestBody Action action) throws NotFoundException {
		log.info("Saving action : {}", action);
		action = actionService.recordNewAction(action);
		return new ResponseEntity<>(action, HttpStatus.OK);
	}

	@PutMapping("/{id}/status/{status}")
	public ResponseEntity<?> updateAnActionStatus(@PathVariable("id") Long id, @PathVariable("status") boolean status) {
		Optional<Action> updatedAction = actionService.updateAction(id, status);
		return updatedAction.<ResponseEntity<?>>map(updatedAction1 -> new ResponseEntity<>(updatedAction1, HttpStatus.OK))
				.orElseGet(() -> new ResponseEntity<>(Constants.ErrorConstants.ACTION_NOT_FOUND, HttpStatus.NOT_FOUND));
	}
}
