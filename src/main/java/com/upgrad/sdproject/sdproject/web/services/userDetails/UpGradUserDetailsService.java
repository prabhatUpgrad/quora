package com.upgrad.sdproject.sdproject.web.services.userDetails;

import com.upgrad.sdproject.sdproject.web.entities.Role;
import com.upgrad.sdproject.sdproject.web.entities.User;
import com.upgrad.sdproject.sdproject.web.services.user.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author prabhat on 31/03/18
 */
@Service
@Transactional(readOnly = true)
@Slf4j
public class UpGradUserDetailsService implements UserDetailsService {

	private final UserService userService;

	@Autowired public UpGradUserDetailsService(UserService userService) {
		this.userService = userService;
	}

	@Override public UserDetails loadUserByUsername(String usernameOrEmail) {
		if (usernameOrEmail == null) {
			log.info("Null Username or Email");
			throw new UsernameNotFoundException("Null Username or Email");
		}

		if (userService == null) {
			log.info("User Service not injected");
			throw new BeanInitializationException("User Service not injected");
		}

		User user = userService.findUserByUsername(usernameOrEmail);
		if (user == null) {
			user = userService.findUserByEmail(usernameOrEmail);
		}
		if (user == null) {
			throw new UsernameNotFoundException(usernameOrEmail);
		}

		List<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
		List<Role> permissions = new ArrayList<Role>(user.getRoles());
		for (Role permission : permissions) {
			grantedAuthorities.add(new SimpleGrantedAuthority(permission.getRoleName()));
		}
		return new UpGradUserDetails(user, grantedAuthorities);
	}
}
