package com.upgrad.sdproject.sdproject.web.services.registration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.upgrad.sdproject.sdproject.web.entities.User;
import com.upgrad.sdproject.sdproject.web.services.user.UserService;
import com.upgrad.sdproject.sdproject.web.services.userDetails.UpGradUserDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author prabhat on 01/04/18
 */
@Service
@Slf4j
public class RegistrationService {

	private final UserService userService;

	@Autowired public RegistrationService(UserService userService) {
		this.userService = userService;
	}

	public String signUp(String json) throws IOException {
		log.info("Processing Signup Request for : {}", json);
		ObjectMapper mapper = new ObjectMapper();
		User user = mapper.readValue(json, User.class);

		//Setting user in userProfile instance for successful Cascading
		user.getUserProfile().setUser(user);
		user = userService.saveUser(user);
		log.info("User created successfully");
		return mapper.writeValueAsString(user);
	}

	public ResponseEntity<?> login(HttpServletRequest request) {
		log.info("Processing login request : {}", request);
		if ((SecurityContextHolder.getContext().getAuthentication() == null)
				|| !(SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof UserDetails)) {
			return new ResponseEntity<>("Invalid Credentials", HttpStatus.UNAUTHORIZED);
		}
		UpGradUserDetails userDetails = (UpGradUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User user = userDetails.getUser();
		log.info("Processed the login request for : {}", user.getEmail());
		return new ResponseEntity<>(user, HttpStatus.OK);
	}
}
