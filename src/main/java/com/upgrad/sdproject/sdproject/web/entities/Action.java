package com.upgrad.sdproject.sdproject.web.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * @author prabhat on 10/06/18
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "actionType", "targetEntityType", "targetId", "createdBy" }) })
public class Action extends Auditable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Enumerated
	private ActionType actionType;
	private boolean isSet;
	@Enumerated
	private TargetEntityType targetEntityType;
	private Long targetId;

	public enum ActionType {
		LIKE, FOLLOW
	}

	public enum TargetEntityType {
		QUESTION, ANSWER, COMMENT;
	}
}
