package com.upgrad.sdproject.sdproject.web.repositories;

import com.upgrad.sdproject.sdproject.web.entities.Category;
import com.upgrad.sdproject.sdproject.web.entities.Question;
import com.upgrad.sdproject.sdproject.web.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author prabhat on 14/04/18
 */
@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {

	Page<Question> findAllByCategories(List<Category> categories, Pageable pageable);

	Page<Question> findAllByUser(User user, Pageable pageable);
}
