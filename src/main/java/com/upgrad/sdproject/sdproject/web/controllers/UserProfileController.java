package com.upgrad.sdproject.sdproject.web.controllers;

import com.upgrad.sdproject.sdproject.utils.SecurityUtil;
import com.upgrad.sdproject.sdproject.web.entities.User;
import com.upgrad.sdproject.sdproject.web.entities.UserProfile;
import com.upgrad.sdproject.sdproject.web.services.userProfile.UserProfileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

import static com.upgrad.sdproject.sdproject.utils.Constants.ErrorConstants.USER_PROFILE_NOT_FOUND;

/**
 * @author prabhat on 12/04/18
 */
@Controller
@Slf4j
@RequestMapping("/userProfile")
public class UserProfileController {

	private final UserProfileService userProfileService;

	@Autowired public UserProfileController(UserProfileService userProfileService) {
		this.userProfileService = userProfileService;
	}

	@GetMapping("/")
	public ResponseEntity<?> getUserProfileForSelf() {
		User user = SecurityUtil.getUser();
		Optional<UserProfile> userProfile = userProfileService.getUserProfileByUser(user);
		return userProfile.<ResponseEntity<?>>map(userProfile1 -> new ResponseEntity<>(userProfile1, HttpStatus.OK))
				.orElseGet(() -> new ResponseEntity<>(USER_PROFILE_NOT_FOUND, HttpStatus.NOT_FOUND));
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> getUserProfileById(@PathVariable("id") Long id) {
		Optional<UserProfile> userProfile = userProfileService.getUserProfileById(id);
		return userProfile.<ResponseEntity<?>>map(userProfile1 -> new ResponseEntity<>(userProfile1, HttpStatus.OK))
				.orElseGet(() -> new ResponseEntity<>(USER_PROFILE_NOT_FOUND, HttpStatus.NOT_FOUND));
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> updateUserProfileById(@PathVariable("id") Long id, @Validated @RequestBody UserProfile updatedProfile) {
		Optional<UserProfile> newUserProfile = userProfileService.updateUserProfile(id, updatedProfile);
		return newUserProfile.<ResponseEntity<?>>map(userProfile1 -> new ResponseEntity<>(userProfile1, HttpStatus.OK))
				.orElseGet(() -> new ResponseEntity<>(USER_PROFILE_NOT_FOUND, HttpStatus.NOT_FOUND));
	}

	@PatchMapping("/{id}")
	public ResponseEntity<?> patchUserProfile(@PathVariable("id") Long id, @RequestParam("patchJson") String json) {
		Optional<UserProfile> updatedUserProfile = userProfileService.patchUserProfile(id, json);
		return updatedUserProfile.<ResponseEntity<?>>map(userProfile1 -> new ResponseEntity<>(userProfile1, HttpStatus.OK))
				.orElseGet(() -> new ResponseEntity<>(USER_PROFILE_NOT_FOUND, HttpStatus.NOT_FOUND));
	}

	//We don't need POST call for UserProfile as it is created along with User.
}
