package com.upgrad.sdproject.sdproject.web.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import java.util.Date;

/**
 * @author prabhat on 11/04/18
 */
@Entity
@Data
@EqualsAndHashCode(callSuper=false)
public class UserProfile extends Auditable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(columnDefinition = "TEXT")
	private String aboutMe;

	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dateOfBirth;

	@Enumerated
	private Gender gender;

	private String contactNumber;

	private String country;

	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", nullable = false)
	private User user;

	@Version
	private int version;

	public UserProfile getUpdatedFromSource(UserProfile source) {
		aboutMe = source.aboutMe;
		dateOfBirth = source.dateOfBirth;
		gender = source.gender;
		contactNumber = source.contactNumber;
		country = source.country;
		return this;
	}
}
