package com.upgrad.sdproject.sdproject.web.repositories;

import com.upgrad.sdproject.sdproject.web.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author prabhat on 31/03/18
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	User findByEmail(String email);

	User findByUserName(String username);
}
