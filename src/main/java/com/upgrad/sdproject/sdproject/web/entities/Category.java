package com.upgrad.sdproject.sdproject.web.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * @author prabhat on 14/04/18
 */
@Entity
@Data
@EqualsAndHashCode(callSuper=false)
public class Category extends Auditable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotNull
	@Column(nullable = false, unique = true, length = 20)
	private String name;

	@NotNull
	@Column(nullable = false)
	private String description;
}
