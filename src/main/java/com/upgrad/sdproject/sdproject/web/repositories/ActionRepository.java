package com.upgrad.sdproject.sdproject.web.repositories;

import com.upgrad.sdproject.sdproject.web.entities.Action;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author prabhat on 10/06/18
 */
@Repository
public interface ActionRepository extends JpaRepository<Action, Long> {
}
