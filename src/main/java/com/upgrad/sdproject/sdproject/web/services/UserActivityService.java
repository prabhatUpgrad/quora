package com.upgrad.sdproject.sdproject.web.services;

import com.upgrad.sdproject.sdproject.utils.SecurityUtil;
import com.upgrad.sdproject.sdproject.web.entities.Action;
import com.upgrad.sdproject.sdproject.web.entities.User;
import com.upgrad.sdproject.sdproject.web.entities.UserActivity;
import com.upgrad.sdproject.sdproject.web.repositories.UserActivitiesRepository;
import com.upgrad.sdproject.sdproject.web.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

/**
 * @author prabhat on 10/06/18
 */
@Service
public class UserActivityService {

	private final UserActivitiesRepository userActivitiesRepository;

	private final UserRepository userRepository;

	@Autowired public UserActivityService(UserActivitiesRepository userActivitiesRepository,
			UserRepository userRepository) {
		this.userActivitiesRepository = userActivitiesRepository;
		this.userRepository = userRepository;
	}

	public void postActivity(UserActivity.ActivityType activityType, Action.TargetEntityType targetEntityType, Long targetId) {
		UserActivity userActivity = new UserActivity();
		userActivity.setActivityType(activityType);
		userActivity.setTargetEntityType(targetEntityType);
		userActivity.setTargetId(targetId);
		userActivity.setUser(SecurityUtil.getUser());
		userActivitiesRepository.save(userActivity);
	}

	public Page<UserActivity> getAllActivities(Pageable pageable) {
		return userActivitiesRepository.findAll(pageable);
	}

	public Page<UserActivity> getAllActivitiesForUser(Long userId, Pageable pageable) {
		User user = userRepository.findById(userId).orElseThrow(EntityNotFoundException::new);
		return userActivitiesRepository.findAllByUser(user, pageable);
	}
}
