package com.upgrad.sdproject.sdproject.web.services;

import com.upgrad.sdproject.sdproject.utils.JsonUtil;
import com.upgrad.sdproject.sdproject.utils.SecurityUtil;
import com.upgrad.sdproject.sdproject.utils.exceptions.BadRequestException;
import com.upgrad.sdproject.sdproject.web.entities.Action;
import com.upgrad.sdproject.sdproject.web.entities.Category;
import com.upgrad.sdproject.sdproject.web.entities.Question;
import com.upgrad.sdproject.sdproject.web.entities.User;
import com.upgrad.sdproject.sdproject.web.entities.UserActivity;
import com.upgrad.sdproject.sdproject.web.repositories.CategoryRepository;
import com.upgrad.sdproject.sdproject.web.repositories.QuestionRepository;
import com.upgrad.sdproject.sdproject.web.repositories.UserRepository;
import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.upgrad.sdproject.sdproject.utils.Constants.ErrorConstants.USER_NOT_FOUND;

/**
 * @author prabhat on 14/04/18
 */
@Service
@Transactional
@Slf4j
public class QuestionService {

	private final QuestionRepository questionRepository;
	private final CategoryRepository categoryRepository;
	private final UserRepository userRepository;
	private final UserActivityService userActivityService;

	@Autowired public QuestionService(QuestionRepository questionRepository, CategoryRepository categoryRepository, UserRepository userRepository,
			UserActivityService userActivityService) {
		this.questionRepository = questionRepository;
		this.categoryRepository = categoryRepository;
		this.userRepository = userRepository;
		this.userActivityService = userActivityService;
	}

	public Optional<Question> getQuestionById(long id) {
		return questionRepository.findById(id);
	}

	public Page<Question> getQuestionsByCategories(List<String> categoryNames, Pageable pageable) {
		List<Category> categories = categoryNames.stream()
				.map(categoryRepository::findByName)
				.filter(Optional::isPresent)
				.map(Optional::get)
				.collect(Collectors.toList());
		return questionRepository.findAllByCategories(categories, pageable);
	}

	public Page<Question> getQuestionsByUser(long userId, Pageable pageable) throws NotFoundException {
		User user = userRepository.findById(userId).orElseThrow(() -> new NotFoundException(USER_NOT_FOUND));
		return questionRepository.findAllByUser(user, pageable);
	}

	public Optional<Question> createNewQuestion(Question question) {
		if (questionRepository.exists(Example.of(question))) {
			return Optional.empty();
		}
		Set<Category> categories = question.getCategories();
		if (categories.isEmpty()) {
			throw new BadRequestException("No category defined for question");
		}
		categories = categories.stream()
				.map(category -> categoryRepository.findByName(category.getName()))
				.filter(Optional::isPresent)
				.map(Optional::get)
				.collect(Collectors.toSet());
		question.setCategories(categories);
		question.setUser(SecurityUtil.getUser());
		Question newQuestion = questionRepository.saveAndFlush(question);
		userActivityService.postActivity(UserActivity.ActivityType.POST_QUESTION, Action.TargetEntityType.QUESTION, newQuestion.getId());
		return Optional.of(newQuestion);
	}

	public Optional<Question> updateQuestion(long id, Question question) {
		Function<Question, Question> function = existingQuestion -> {
			existingQuestion.setTitle(question.getTitle());
			existingQuestion.setContent(question.getContent());
			existingQuestion.setUser(question.getUser());
			existingQuestion.setCategories(question.getCategories().stream()
					.map(category -> categoryRepository.findByName(category.getName()))
					.filter(Optional::isPresent)
					.map(Optional::get)
					.collect(Collectors.toSet()));
			return existingQuestion;
		};
		try {
			return editQuestion(id, function);
		} catch (Exception e) {
			throw new BadRequestException("Patch request for question failed", e);
		}
	}

	public Optional<Question> patchQuestion(long id, String updatedJson) {
		Function<Question, Question> function = existingQuestion -> {
			String patchJson = JsonUtil.patchJson(JsonUtil.toJson(existingQuestion), updatedJson);
			return JsonUtil.fromJson(patchJson, Question.class);
		};
		return editQuestion(id, function);
	}

	private Optional<Question> editQuestion(Long id, Function<Question, Question> function) {
		Optional<Question> existingQuestion = questionRepository.findById(id);
		if (existingQuestion.isPresent()) {
			Question question = existingQuestion.get();
			question = function.apply(question);
			return Optional.of(questionRepository.save(question));
		}
		return Optional.empty();
	}
}
